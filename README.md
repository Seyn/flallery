# Resumen
- Me habría gustado poder ir escribiendo el proceso, pero no he tenido demasiado tiempo, así que he intentado que en los commits
queden bien explicadas las features que se han ido aplicando.

- Por lo que he visto la api de flickr es un poco liosa, y por lo que me ha parecido a veces faltan recursos en algunas requests.
- Por ejemplo al obtener toda una galería puedo seleccionar unas URL de las imagenes de diferente tamaño, luego al hacer una request a /getInfo no me devuelve la imagen en si, solo la información del post
y sus relaciones. Quizás me he saltado algo y no lo he visto, puede que haya otra manera.

- También comentar que hace casi dos años que no hacía nada con Backbone, y puede que algo no haya estado del todo bien planteado, si fuera así me gustaria que me lo notificarais para mejorarlo a la siguiente vez.

- Me habría gustado hacer un par de cosas más, como por ejemplo unas flechas de navegación cuando la galería está abierta, o algunos efectos mejores o un layout general más elaborado.. Pero como no me ha sido posible.
Features que tal como está ahora no seria dificil de aplicar


## Muchas gracias.
##### - Albert Ramos
