requirejs.config({
    baseUrl: '../src/js/app',
    paths: {
        app: 'app',
        jquery: '../lib/jquery.min',
        underscore: '../lib/underscore.min',
        backbone: '../lib/backbone.min'
    },

    shim: {
        'jquery': {
            exports: '$'
        },
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'backbone'
        },
    }
});

// Start loading the main app file.
requirejs(['app']);
