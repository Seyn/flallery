define(function (require) {
    var Backbone = require('backbone');

    var GalleryModel = Backbone.Model.extend({
        url: null,

        defaults: {
            id: '',
            owner: '',
            title: '',
            url_m: '',
            url_l: '',
            ownername: ''
        },

        initialize: function(options){
            this.url = this.buildUrl();
            this.loadedModel = options.loadedModelAttributes;
        },

        getPostUrl: function() {
            return this.get('urls').url[0]._content;
        },

        parse: function(response) {
            this.model = Object.assign(this.loadedModel, response.photo);
            return this.model;
        },

        buildUrl: function() {
            var api_key = "4d792db12f9eaeb097124b0349e71deb";
            var gallery_id = "72157696968632245";
            var url = "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key="+api_key+"&gallery_id="+gallery_id+"&photo_id="+this.id+"&format=json&nojsoncallback=1";
            return url;
        },
    });

    return GalleryModel;
});