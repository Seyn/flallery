define(function (require) {
    var Backbone = require('backbone');
    var GalleryModel = require('models/gallery');

    var GalleryCollection = Backbone.Collection.extend({
        url: null,
        model: GalleryModel,

        initialize: function() {
            this.url = this.buildUrl();
        },

        buildUrl: function() {
            var api_key = "4d792db12f9eaeb097124b0349e71deb";
            var gallery_id = "72157696968632245";
            var url = "https://api.flickr.com/services/rest/?method=flickr.galleries.getPhotos&api_key="+api_key+"&gallery_id="+gallery_id+"&format=json&nojsoncallback=1&extras=url_m,url_l,owner_name,views";
            return url;
        },

        parse: function(response) {
            var photos = [];

            _.each(response.photos.photo, function(photo){
                var model = new GalleryModel(photo);
                photos.push(model)
            });

            this.add(photos);

            return this.models;
        }
    });

    return GalleryCollection;
});