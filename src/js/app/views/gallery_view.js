define(function (require) {
    var _ = require('underscore');
    var Backbone = require('backbone');
    var GalleryItemView = require('views/gallery_item_view');

    var template = _.template($("#gallery_template").html());

    var GalleryView = Backbone.View.extend({

        el: $('#gallery'),

        collection: null,

        events: {
            'click .gallery-item': 'openModal'
        },

        initialize: function(options){
            this.collection = options.collection;
            this.collection.bind('add', this.render, this);
            this.render();
        },

        /*openModal: function(event) {
            var id = $(event.target).closest('.gallery-item').data('id');
            var model = new GalleryModel({
                id: id
            });
            model.fetch();

            new ModalView({
                model: model
            });
        },*/

        render: function() {
            var compiled_template = template({ collection: this.collection });
            this.$el.hide().html( compiled_template).fadeIn();

            this.onRender();
            return this;
        },

        onRender: function() {
            _.each(this.collection.models, function(model){
                var item = new GalleryItemView({
                    model: model
                });

                this.$('.gallery-container').append(item.render().el);
            });
        }

    });

    return GalleryView;
});