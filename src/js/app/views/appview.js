define(function (require) {
    var _ = require('underscore');
    var Router = require('router');
    var GalleryView = require('views/gallery_view');
    var GalleryCollection = require('collections/gallery');

    var AppView = Backbone.View.extend({

        el: $('body'),

        events: {

        },

        initialize: function(){
            this.router = new Router();

            // Implemented routes
            //Backbone.history.start({ pushState: false, root: '/' });

            this.initGallery();
        },

        initGallery: function() {
            var galleryCollection = new GalleryCollection();
            galleryCollection.fetch({
                success: function() {
                    new GalleryView({
                        collection: galleryCollection
                    });
                }
            });


        }

    });

    return AppView;
});