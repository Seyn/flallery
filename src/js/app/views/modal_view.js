define(function (require) {
    var _ = require('underscore');
    var Backbone = require('backbone');
    var template = _.template($("#model_template").html());

    var ModalView = Backbone.View.extend({

        el: '.image-container',

        model: null,

        events: {
            'click .close-modal': 'close'
        },

        initialize: function(options){
            this.model = options.model;
            this.render();
        },

        close: function(){
            this.$el.closest('#modal-container').removeClass('open');
        },

        render: function() {
            var compiled_template = template({ model: this.model });
            this.$el.closest('#modal-container').addClass('open');
            this.$el.html(compiled_template);

            return this;
        }

    });

    return ModalView;
});