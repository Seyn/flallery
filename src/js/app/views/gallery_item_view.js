define(function (require) {
    var _ = require('underscore');
    var ModalView = require('views/modal_view');
    var GalleryModel = require('models/gallery');

    var template = _.template($("#gallery_item_template").html());

    var GalleryItemView = Backbone.View.extend({

        className: 'gallery-item-container',

        model: null,

        events: {
            'click .gallery-item': 'openModal'
        },

        initialize: function(options){
            this.model = options.model;
            this.model.bind('change', this.render, this);

            this.render();
        },

        openModal: function(event) {
            var _self = this;
            var id = $(event.target).closest('.gallery-item').data('id');
            var model = new GalleryModel({
                id: id,
                loadedModelAttributes: _self.model.attributes
            });

            model.fetch({
                success: function(){
                    new ModalView({
                        model: model
                    });
                }
            });


        },

        render: function() {
            var compiled_template = template({ model: this.model });
            this.$el.html( compiled_template);

            return this;
        }

    });

    return GalleryItemView;
});